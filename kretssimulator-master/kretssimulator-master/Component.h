#ifndef COMPONENT_H
#define COMPONENT_H
#include "Connection.h"
#include <string>
class Component
{
public:
    virtual void simulate(double time) = 0;
    virtual double getVoltage() const;
    virtual double getCurrent() const{ return 0; };
    std::string getName() const { return name; };
    double getInPotential() const { return in_connection->getPotential();};
    double getOutPotential() const { return out_connection->getPotential();};
	
protected:
    Component(const std::string& name, Connection& in, Connection& out);
    
    std::string name{"nonname"};
    Connection* in_connection = nullptr;
    Connection* out_connection = nullptr;

private:
};
#endif
