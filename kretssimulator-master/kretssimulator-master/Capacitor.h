#ifndef CAPACITOR_H
#define CAPACITOR_H
#include <string>
#include "Connection.h"
#include "Component.h"
class Capacitor : public Component
{
public:
    Capacitor(const std::string& name, double capacitance, Connection& in,
	      Connection& out);
    double getCurrent() const;
    double getCharge() const{ return charge; };
    double getCapacitance() const{ return capacitance; };
    void simulate(double time);
private:
    double charge{ 0 }, capacitance{ 0 };
};
#endif
