#include "Component.h"
#include "Connection.h"
#include <string>
Component::Component(const std::string& name ,Connection& in, Connection& out) :  name{name},in_connection{&in}, out_connection{&out}{}

double Component::getVoltage() const
{
    double voltage = in_connection->getPotential()
	- out_connection->getPotential();
    if (voltage < 0)
    {
	voltage = voltage * -1;
    }
    return voltage;
}
