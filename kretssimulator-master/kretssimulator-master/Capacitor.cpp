#include "Capacitor.h"
#include <string>
#include "Connection.h"
#include "Component.h"

Capacitor::Capacitor(const std::string& name, double capacitance, Connection& in,
		     Connection& out) : Component(name, in, out),
					capacitance{capacitance}{}
void Capacitor::simulate(double time)
{
    double temp = getCapacitance()*(getVoltage() - getCharge())*time;
    if (in_connection->getPotential() > out_connection->getPotential())
    {
	charge += temp;
	in_connection->decreasePotential(temp);
	out_connection->increasePotential(temp);
    }
    else if (in_connection->getPotential() < out_connection->getPotential())
    {
	charge += temp;
	out_connection->decreasePotential(temp);
	in_connection->increasePotential(temp);
    }
}

double Capacitor::getCurrent() const
{
    return getCapacitance()*(getVoltage() - getCharge());
}
