#ifndef RESISTOR_H
#define RESISTOR_H
#include <string>
#include "Component.h"
#include "Connection.h"
class Resistor : public Component
{
public:
    Resistor(const std::string& Name, double Resistance, Connection& in, Connection& out);
    void simulate(double time);
    double getCurrent() const;
    double getResistance() const { return resistance; };
private:
    double resistance{0};
};
#endif
