#include "Battery.h"
#include <iostream>
#include <string>
#include "Connection.h"
#include "Component.h"

Battery::Battery(const std::string& name, double voltage, Connection& in,
		 Connection& out) : Component(name, in, out), voltage{voltage}{}

void Battery::simulate(double time)
{
    out_connection->setPotential(getVoltage());
    in_connection->setPotential(0);
}

double Battery::getVoltage() const
{
    return voltage;
}
