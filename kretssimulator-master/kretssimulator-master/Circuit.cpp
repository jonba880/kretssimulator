#include "Circuit.h"
#include "Component.h"
#include <iomanip>
#include <iostream>

using namespace std;
Circuit::Circuit(std::string name) : circuitName{name}
{
}

void Circuit::addComponent(Component *c)
{
    components.push_back(c);
}

void Circuit::print() const
{
    for (Component* c : components) 
    {
	cout <<  setw(6)<<  fixed <<  setprecision(2) <<  setw(6)
	     << c->getVoltage() <<  setw(6) << c->getCurrent();
    }
    cout <<  "\n";
}

void Circuit::simulate(int loopCount, int rows, double time) 
{
    int whenToPrint = round(1.0*loopCount / rows);
    cout << "\n"<< circuitName <<  ":\n";
    for (Component *c : components)
    {
	cout <<  setw(12) << c->getName() ;
    }
    cout << "\n";
    for (uint i= 0; i<components.size();i++) 
    {
	cout <<  setw(6) << "Volt" <<  setw(6) <<  "Curr";
    }
    cout << "\n";
    for (int i = 1; i < loopCount+1; i++) 
    {
	for (Component *c : components) 
	{
	    c->simulate(time);
	}
	if (i%whenToPrint == 0) 
	{
	    print();
	}
    }
}
