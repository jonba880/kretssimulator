#include "Connection.h"
#include "Component.h"
#include "Battery.h"
#include "Circuit.h"
#include "Capacitor.h"
#include <math.h>
#include "Resistor.h"
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>


int main(int argc, char* argv[])
{
    int iterations{ 0 }, rows{ 0 };
    double time{ 0 }, batteryVoltage{ 0 };
    if (argc == 5) 
    {
	try
	{
	    iterations = std::stoi(argv[1]);
	    rows = std::stoi(argv[2]);
	    time = std::stod(argv[3]);
	    batteryVoltage = std::stod(argv[4]);
	}
	catch (std::invalid_argument)
	{
	    std::cout << "Invalid argument" << std::endl;
	    return EXIT_FAILURE;
	}
	catch (std::out_of_range)
	{
	    std::cout << "Argument out of range" << std::endl;
	    return EXIT_FAILURE;
	}
	catch (...) 
	{
	    std::cout << "Exception!" << std::endl;
	}
    }
    else
    {
	std::cout << "Wrong number of arguments" << std::endl;
	return EXIT_FAILURE;
    }

    Connection e, f, g, h;
    Circuit krets1("Krets1");
    krets1.addComponent(new Battery("Bat", batteryVoltage, f, e));
    krets1.addComponent(new Resistor("R1", 6.0, e, g));
    krets1.addComponent(new Resistor("R2", 4.0, g, h));
    krets1.addComponent(new Resistor("R3", 8.0, h, f));
    krets1.addComponent(new Resistor("R4", 12.0, g, f));
    krets1.simulate(iterations, rows, time);
	
    Connection a,b,c,d;
    Circuit krets2("Krets2");
    krets2.addComponent(new Battery("Bat", batteryVoltage, d, a));
    krets2.addComponent(new Resistor("R1", 150, a, b));
    krets2.addComponent(new Resistor("R2", 50, a, c));
    krets2.addComponent(new Resistor("R3", 100, b, c));
    krets2.addComponent(new Resistor("R4", 300, b, d));
    krets2.addComponent(new Resistor("R5", 250, c, d));
    krets2.simulate(iterations, rows, time);
	
    Connection p,l,r,n;
    Circuit krets3("Krets3");
    krets3.addComponent(new Battery("Bat", batteryVoltage, n, p));
    krets3.addComponent(new Resistor("R1", 150, p, l));
    krets3.addComponent(new Resistor("R2", 50, p, r));
    krets3.addComponent(new Capacitor("C3", 1, l, r));
    krets3.addComponent(new Resistor("R4", 300, l, n));
    krets3.addComponent(new Capacitor("C5", 0.75, r, n));
    krets3.simulate(iterations, rows, time); 

    return EXIT_SUCCESS;
}
