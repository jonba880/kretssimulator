#include "Resistor.h"
#include "Connection.h"
#include "Component.h"

Resistor::Resistor(const std::string& name, double resistance, Connection& in,
		   Connection& out) : Component(name, in, out), resistance{resistance}{}


void Resistor::simulate(double time)
{
    //MINSKA/�KA potentialen i de olika punkterna
    if (in_connection->getPotential() > out_connection->getPotential())
    {
	in_connection->decreasePotential(getVoltage()/getResistance()*time);
	out_connection->increasePotential(getVoltage() / getResistance()*time);
    }
    else if (in_connection->getPotential() < out_connection->getPotential())
    {
	out_connection->decreasePotential(getVoltage() / getResistance()*time);
	in_connection->increasePotential(getVoltage() / getResistance()*time);
    }
}

double Resistor::getCurrent() const
{
    return (getVoltage() / getResistance());
}

