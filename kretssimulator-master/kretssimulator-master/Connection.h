#ifndef CONNECTION_H
#define CONNECTION_H
class Connection{
public:
    Connection(){};
    double getPotential() const { return potential; };
    void setPotential(double potential) { this->potential = potential; };
    void increasePotential(double potential) { this->potential += potential; };
    void decreasePotential(double potential) { this->potential -= potential; };
private:
    double potential{0};
};
#endif



