#ifndef Circuit_H
#define Circuit_H
#include <vector>
#include <iomanip>
#include <math.h>
#include <iostream>
#include "Component.h"

class Circuit 
{

public:
    Circuit(std::string name);
    void print() const;
    void addComponent(Component *c);
    void simulate(int iterations, int rows, double time);
private:
    std::vector<Component*> components;
    std::string circuitName{""};
};

#endif
