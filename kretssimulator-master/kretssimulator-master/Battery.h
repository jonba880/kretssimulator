#ifndef BATTERY_H
#define BATTERY_H
#include <string>
#include "Component.h"
#include "Connection.h"
class Battery : public Component
{
public:
    Battery(const std::string& name, double voltage, Connection& in,
	    Connection& out);
    void simulate(double time);
    double getVoltage() const;
private:
    double voltage{0};
};
#endif
